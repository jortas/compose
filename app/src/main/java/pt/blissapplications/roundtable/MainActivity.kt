package pt.blissapplications.roundtable

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ComposeView(this).also {
            setContentView(it)
        }.setContent {
            MaterialTheme() {
                Column() {
                    Button(
                        onClick = ::startEx1Activity,
                        modifier = buttonStyle
                    ) {
                        Text(text = "Ex1")
                    }

                    Button(
                        onClick = ::startEx2Activity,
                        modifier = buttonStyle
                    ) {
                        Text(text = "Ex2")
                    }
                }
            }

        }
    }

    val buttonStyle = Modifier.padding(16.dp).height(64.dp).fillMaxWidth()

    private fun startEx1Activity() {
        startActivity(Intent(this, Ex1Activity::class.java))
    }

    private fun startEx2Activity() {
        startActivity(Intent(this, Ex2Activity::class.java))
    }
}