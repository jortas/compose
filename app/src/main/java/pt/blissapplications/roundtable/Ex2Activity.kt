package pt.blissapplications.roundtable

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.delay

class Ex2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var x = MutableLiveData(0f)
        var y = MutableLiveData(0f)

        ComposeView(this).also {
            setContentView(it)
        }.setContent {
            MaterialTheme {

                val xx = x.observeAsState()
                val yy = y.observeAsState()

                Box(
                    Modifier
                        .background(Color.Black)
                        .padding(8.dp)
                ) {


                    Circle(Modifier.size(64.dp), xx.value!!, yy.value!!)
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            for (i in 180 downTo 50) {
                delay(5)
                val p = i.toDouble() / 100.0
                Log.d("X", " $i  //  $p --|> x: ${positionX(p)}")

                x.value = positionX(Math.toRadians(i.toDouble())).toFloat()
                y.value = positionY(Math.toRadians(i.toDouble())).toFloat()
            }
        }
    }

    fun positionX(progress: Double): Double {
        return 600 + Math.cos(progress) * 400
    }

    fun positionY(progress: Double): Double {
        return 600 - Math.sin(progress) * 400
    }

    @Composable
    fun Circle(modifier: Modifier = Modifier, x: Float, y: Float) {
        Canvas(modifier = modifier, onDraw = {
            drawCircle(
                center = Offset(x, y),
                color = Color.Yellow
            )
        })
    }
}