package pt.blissapplications.roundtable

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.*
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalAnimationApi
class Ex1Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ComposeView(this).also {
            setContentView(it)
        }.setContent {

            MaterialTheme() {
                // Create a MutableTransitionState<Boolean> for the AnimatedVisibility.
                val state = remember {
                    MutableTransitionState(false).apply {
                        // Start the animation immediately.
                        targetState = true
                    }
                }
                Box(
                    contentAlignment = Alignment.Center
                ) {

                    val state = remember {
                        MutableTransitionState(false).apply {
                        }
                    }

                    val composableScope = rememberCoroutineScope()

                    composableScope.launch {
                        delay(500)
                        state.targetState = true
                        delay(2000)
                        state.targetState = false
                    }

                    Column {
                        AnimatedVisibility(
                            visibleState = state,
                            enter = fadeIn(animationSpec = tween(1000)) + slideInVertically(),
                            exit = fadeOut()
                        ) {
                            // this: AnimatedVisibilityScope
                            // Use AnimatedVisibilityScope.transition() to add a custom animation
                            // to the AnimatedVisibility.
                            Image(
                                painterResource(R.drawable.ic_android),
                                contentDescription = "",
                                modifier = Modifier
                                    .size(200.dp)
                                    .background(Color.Black),
                                colorFilter = ColorFilter.tint(
                                    Color.Green,
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}